using Godot;
using System;
using System.Collections.Generic;

public class Inventory : Node
{
	[Export] public PackedScene ResourcePack;

	[Signal] public delegate void UpdateMetal(int amount);
	[Signal] public delegate void UpdateCircuitry(int amount);
	[Signal] public delegate void UpdateEnergy(int amount);
	[Signal] public delegate void UpdateCred(int amount);
	[Signal] public delegate void UpdateBackpack(int index, Node item);
	[Signal] public delegate void UpdateBackpackSize(int size);
	
	public int BackpackSize = 4;
	public int Metal = 0;
	public int Circuitry = 0;
	public int Energy = 0;
	public int Cred = 0;
	public Node[] Backpack = new Node[0];
	
	public override void _Ready()
	{
		UpgradeBackpack(BackpackSize);
	}
	
	public void UpgradeBackpack(int size)
	{
		BackpackSize = size;
		Array.Resize(ref Backpack, size);
		EmitSignal("UpdateBackpackSize", size);
	}

	public void CollectMetal(int amount)
	{
		Metal += amount;
		EmitSignal("UpdateMetal", Metal);
	}

	public void SpendMetal(int amount)
	{
		Metal -= amount;
		EmitSignal("UpdateMetal", Metal);
	}

	public void CollectCircuitry(int amount)
	{
		Circuitry += amount;
		EmitSignal("UpdateCircuitry", Circuitry);
	}

	public void SpendCircuitry(int amount)
	{
		Circuitry -= amount;
		EmitSignal("UpdateCircuitry", Circuitry);
	}

	public void CollectEnergy(int amount)
	{
		Energy += amount;
		EmitSignal("UpdateEnergy", Energy);
	}

	public void SpendEnergy(int amount)
	{
		Energy -= amount;
		EmitSignal("UpdateEnergy", Energy);
	}

	public void CollectCred(int amount)
	{
		Cred += amount;
		EmitSignal("UpdateCred", Cred);
	}

	public void SpendCred(int amount)
	{
		Cred -= amount;
		EmitSignal("UpdateCred", Cred);
	}
	
	public bool PickupItem(Node item)
	{
		var added = false;
		for(int i = 0; i < BackpackSize && !added; i++)
		{
			if(Backpack[i] == null)
			{
				added = true;
				Backpack[i] = item;
				item.GetNode<Timer>("DespawnTimer").Stop();
				EmitSignal("UpdateBackpack", i, item);
			}
		}
		
		return added;
	}
	
	public Node DropItem(int index)
	{
		var dropped = Backpack[index];
		Backpack[index] = null;
		EmitSignal("UpdateBackpack", index, null);
		return dropped;
	}
	
	public Node DropResources(int metalAmount, int circuitryAmount, int energyAmount, int credAmount)
	{
		var resources = (Area2D)ResourcePack.Instance();
		if(metalAmount > 0 && metalAmount <= Metal)
		{
			resources.Set("Metal", metalAmount);
			SpendMetal(metalAmount);
		}
		if(circuitryAmount > 0 && circuitryAmount <= Circuitry)
		{
			resources.Set("Circuitry", circuitryAmount);
			SpendCircuitry(circuitryAmount);
		}
		if(energyAmount > 0 && energyAmount <= Energy)
		{
			resources.Set("Energy", energyAmount);
			SpendEnergy(energyAmount);
		}
		if(credAmount > 0 && credAmount <= Cred)
		{
			resources.Set("Cred", credAmount);
			SpendCred(credAmount);
		}
		
		if((int)resources.Get("Metal") + 
			(int)resources.Get("Circuitry") + 
			(int)resources.Get("Energy") + 
			(int)resources.Get("Cred") > 0)
		{
			return resources;
		}
		else
		{
			resources.QueueFree();
			return null;
		}
	}
	
	public List<Node> DropAll()
	{
		// Backpack.Where(item => item != null).ToArray() doesn't work ??
		var droppedItems = new List<Node>();
		for(int i = 0; i < BackpackSize; i++)
		{
			if(Backpack[i] != null)
			{
				droppedItems.Add(Backpack[i]);
				EmitSignal("UpdateBackpack", i, null);
			}
		}
		
		Backpack = new Node[BackpackSize];
		
		// Add Resources to dropped items
		var resources = DropResources(Metal, Circuitry, Energy, Cred);
		if(resources != null)
			droppedItems.Add(resources);
		
		return droppedItems;
	}
	
	public void DestroyAll()
	{
		Backpack = new Node[BackpackSize];
		Metal = 0;
		Circuitry = 0;
		Energy = 0;
		Cred = 0;

		for(int i = 0; i < BackpackSize; i++)
		{
			EmitSignal("UpdateBackpack", i, null);
		}
		EmitSignal("UpdateMetal", 0);
		EmitSignal("UpdateCircuitry", 0);
		EmitSignal("UpdateEnergy", 0);
		EmitSignal("UpdateCred", 0);
	}
}
