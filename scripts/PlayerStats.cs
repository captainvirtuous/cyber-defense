using Godot;
using System;
using System.Collections.Generic;

public class PlayerStats : Node
{
	[Signal] public delegate void ModifyHealth(int amount);
	[Signal] public delegate void ModifyMaxHP();
	[Signal] public delegate void ModifyMaxStamina();
	[Signal] public delegate void WeaponRange(int weaponRange, string weaponRangeShape);

	public static int Attack = 10;
	public static int Defense = 0;
	public static int Heal = 5;
	public static int MaxHP = 100;
	public static int Speed = 400;

	public static int MaxStamina = 100;
	public static int StaminaDrain = 10;
	public static int StaminaRegen = 10;

	public static int BuildRange = 192;
	public static int EquipPrimary = 0;

	public static bool Killed = true;
	
	public List<string> Equipment = new List<string>();
	public Node2D Weapon;
	public int BaseWeaponRange = 4;
	public string BaseWeaponRangeShape = "Rectangle";
	
	public void ModifyPlayerHealth(int amount)
	{
		EmitSignal("ModifyHealth", amount);
	}
	
	public void ModifyPlayerStats(string stat, int modifier)
	{
		if (modifier != 0)
		{
			switch(stat)
			{
				case "Attack": Attack += modifier; break;
				case "Defense": Defense += modifier; break;
				case "Heal": Heal += modifier; break;
				case "Speed": Speed += modifier; break;
				case "StaminaDrain": StaminaDrain += modifier; break;
				case "StaminaRegen": StaminaRegen += modifier; break;
				case "BuildRange": BuildRange += modifier; break;
				case "MaxHP":
					MaxHP += modifier;
					EmitSignal("ModifyMaxHP");
					break;
				case "MaxStamina":
					MaxStamina += modifier;
					EmitSignal("ModifyMaxStamina");
					break;
			}
		}
	}
	
	public bool CheckSlotEquipped(string slot)
	{
		return Equipment.Contains(slot);
	}
	
	public void EquipSlot(string slot)
	{
		Equipment.Add(slot);
	}
	
	public void UnequipSlot(string slot)
	{
		Equipment.Remove(slot);
	}
	
	public void EquipWeapon(Node2D item)
	{
		Weapon = item;
		EmitSignal("WeaponRange", item.Get("WeaponRange"), item.Get("WeaponRangeShape"));
	}
	
	public void UnequipWeapon()
	{
		Weapon = null;
		EmitSignal("WeaponRange", BaseWeaponRange, BaseWeaponRangeShape);
	}

	private void OnInventoryUpdateMetal(int amount)
	{
		GetNode("/root/GUIData").Call("SetMetal", amount);
	}

	private void OnInventoryUpdateCircuitry(int amount)
	{
		GetNode("/root/GUIData").Call("SetCircuitry", amount);
	}

	private void OnInventoryUpdateEnergy(int amount)
	{
		GetNode("/root/GUIData").Call("SetEnergy", amount);
	}

	private void OnInventoryUpdateCred(int amount)
	{
		GetNode("/root/GUIData").Call("SetCred", amount);
	}

	private void OnInventoryUpdateBackpack(int index, Node item)
	{
		if (item != null)
			item.Set("BackpackSlot", index);
		GetNode("/root/GUIData").Call("SetBackpack", index, item);
	}

	private void OnInventoryUpdateBackpackSize(int size)
	{
		GetNode("/root/GUIData").Call("SetBackpackSize", size);
	}
}
