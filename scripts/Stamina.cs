using Godot;
using System;

public class Stamina : ProgressBar
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		if(GetParent().Get("MaxStamina") == null)
			this.MaxValue = 0;
		else
			this.MaxValue = (int)GetParent().Get("MaxStamina");
		this.Value = this.MaxValue;
	}

	// Drain stamina and return if it's empty
	public bool Drain(float drainValue)
	{
		if (this.Value > 0)
			this.Value -= drainValue;
		return this.Value <= 0;
	}

	// Regen stamina and return if it's full
	public bool Regen(float regenValue)
	{
		if (this.Value < this.MaxValue)
			this.Value += regenValue;
			return this.Value >= this.MaxValue;
	}
	
	public void UpdateMaxStamina(int stamina, bool fill)
	{
		this.MaxValue = stamina;
		if (fill)
			this.Value = this.MaxValue;
	}
}
