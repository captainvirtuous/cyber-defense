using Godot;
using System;

public class ShopItem : Button
{
	[Signal] public delegate void Purchase(Node item);

	public Node Item;
	
	public void SetItem(Node item)
	{
		Item = item;
		GetNode<Sprite>("ItemSprite").Texture = item.GetNode<Sprite>("Sprite").Texture;
		GetNode<Sprite>("ItemSprite").RegionRect = item.GetNode<Sprite>("Sprite").RegionRect;
		GetNode<Sprite>("ItemSprite").RegionEnabled = item.GetNode<Sprite>("Sprite").RegionEnabled;
		GetNode<Label>("ItemName").Text = (string)item.Get("ItemName");
		GetNode<Label>("ItemCost").Text = (string)item.Get("CredValue").ToString();
	}

	private void OnShopItemPressed()
	{
		EmitSignal("Purchase", Item);
	}
}
