using Godot;
using System;

public class Shop : StaticBody2D
{
	[Signal] public delegate void OpenShop();
	[Signal] public delegate void CloseShop();
	[Signal] public delegate void Purchase(Node item);
	
	[Export] public PackedScene ShopItem;
	
	private bool ShopOpen = false;
	private bool PlayerAtShop = false;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// Give shop some creds
		GetNode("Inventory").Call("CollectCred", 5000);
		
		// Add Gun to shop
		var gun = (Node2D)GetNode("/root/ItemLibrary").Call("GetItem", 1); // Gun
		gun.Set("ItemName", "Not A Gun");
		var shopItem1 = (Button)ShopItem.Instance();
		shopItem1.RectPosition = new Vector2(4, 4);
		shopItem1.Call("SetItem", gun);
		shopItem1.Connect("Purchase", this, "OnShopItemPurchase");
		GetNode("ShopPanel").AddChild(shopItem1);
		
		// Add Medkit to shop
		var medkit = (Node2D)GetNode("/root/ItemLibrary").Call("GetItem", 2); // Medkit
		var shopItem2 = (Button)ShopItem.Instance();
		shopItem2.RectPosition = new Vector2(68, 4);
		shopItem2.Call("SetItem", medkit);
		shopItem2.Connect("Purchase", this, "OnShopItemPurchase");
		GetNode("ShopPanel").AddChild(shopItem2);
		
		// Add Metal to shop
		var resourcePack1 = (Node2D)GetNode("/root/ItemLibrary").Call("GetItem", 0); // Metal pack
		resourcePack1.Set("ItemName", "50 Metal");
		resourcePack1.Set("CredValue", 100);
		resourcePack1.Set("Metal", 50);
		var shopItem3 = (Button)ShopItem.Instance();
		shopItem3.RectPosition = new Vector2(132, 4);
		shopItem3.Call("SetItem", resourcePack1);
		shopItem3.Connect("Purchase", this, "OnShopItemPurchase");
		GetNode("ShopPanel").AddChild(shopItem3);
		
		// Add Circuitry to shop
		var resourcePack2 = (Node2D)GetNode("/root/ItemLibrary").Call("GetItem", 0); // Circuitry pack
		resourcePack2.Set("ItemName", "25 Circuitry");
		resourcePack2.Set("CredValue", 100);
		resourcePack2.Set("Circuitry", 25);
		var shopItem4 = (Button)ShopItem.Instance();
		shopItem4.RectPosition = new Vector2(200, 4);
		shopItem4.Call("SetItem", resourcePack2);
		shopItem4.Connect("Purchase", this, "OnShopItemPurchase");
		GetNode("ShopPanel").AddChild(shopItem4);
		
		// Add Energy to shop
		var resourcePack3 = (Node2D)GetNode("/root/ItemLibrary").Call("GetItem", 0); // Energy pack
		resourcePack3.Set("ItemName", "25 Energy");
		resourcePack3.Set("CredValue", 100);
		resourcePack3.Set("Energy", 25);
		var shopItem5 = (Button)ShopItem.Instance();
		shopItem5.RectPosition = new Vector2(264, 4);
		shopItem5.Call("SetItem", resourcePack3);
		shopItem5.Connect("Purchase", this, "OnShopItemPurchase");
		GetNode("ShopPanel").AddChild(shopItem5);
		
		GetNode<Panel>("ShopPanel").Hide();
	}

	public void PlayerCollisionBefore(float delta)
	{
		PlayerAtShop = false;
	}

	public void PlayerCollision(Node body, float delta)
	{
		PlayerAtShop = true;
	}

	public void PlayerCollisionAfter(float delta)
	{
		if (PlayerAtShop && !ShopOpen)
		{
			ShopOpen = true;
			GetNode<Panel>("ShopPanel").Show();
			EmitSignal("OpenShop");
		}
		else if (!PlayerAtShop && ShopOpen)
		{
			ShopOpen = false;
			GetNode<Panel>("ShopPanel").Hide();
			EmitSignal("CloseShop");
		}
	}
	
	private void OnShopItemPurchase(Node item)
	{
		EmitSignal("Purchase", item);
	}

	private void OnInventoryUpdateCred(int amount)
	{
		GetNode("ShopPanel").GetNode<Label>("CredAmount").Text = amount.ToString() + " Cred";
	}
}
