using Godot;
using System;

public class GUIData : Node
{
	[Signal] public delegate void WaveLabel(string text);
	[Signal] public delegate void WaveCountdownShow();
	[Signal] public delegate void WaveCountdownHide();
	[Signal] public delegate void MessageLabel(string text);
	[Signal] public delegate void MessageHide();
	[Signal] public delegate void SelectedBuildingSprite(AnimatedSprite sprite);
	[Signal] public delegate void MetalAmountLabel(string text);
	[Signal] public delegate void CircuitryAmountLabel(string text);
	[Signal] public delegate void EmergyAmountLabel(string text);
	[Signal] public delegate void CredAmountLabel(string text);
	[Signal] public delegate void AddBackpackButton(Button itemButton);
	[Signal] public delegate void RemoveItem(int index);

	private Timer WaveTimer;
	private Node[] BackpackButtons = new Node[0];
	
	public void SetWave(int wave)
	{
		EmitSignal("WaveLabel", "Wave " + wave);
	}

	public void SetWaveCountdown(Timer timer)
	{
		WaveTimer = timer;
		EmitSignal("WaveCountdownShow");
	}

	public void HideWaveCountdown()
	{
		EmitSignal("WaveCountdownHide");
	}

	public void SetMessage(string message, float time)
	{
		EmitSignal("MessageLabel", message);
		GetNode<Timer>("MessageKillTimer").Stop();
		
		if (time > 0f)
		{
			GetNode<Timer>("MessageKillTimer").WaitTime = time;
			GetNode<Timer>("MessageKillTimer").Start();
		}
	}

	public void HideMessage()
	{
		EmitSignal("MessageHide");
	}
	
	public void SetBuilding(AnimatedSprite sprite)
	{
		EmitSignal("SelectedBuildingSprite", sprite);
	}
	
	public void SetMetal(int amount)
	{
		EmitSignal("MetalAmountLabel", amount.ToString());
	}
	
	public void SetCircuitry(int amount)
	{
		EmitSignal("CircuitryAmountLabel", amount.ToString());
	}
	
	public void SetEnergy(int amount)
	{
		EmitSignal("EmergyAmountLabel", amount.ToString());
	}
	
	public void SetCred(int amount)
	{
		EmitSignal("CredAmountLabel", amount.ToString());
	}
	
	public void SetBackpack(int index, Node item)
	{
		if (item == null)
		{
			if (BackpackButtons[index] != null)
				BackpackButtons[index].QueueFree();
		}
		else
		{
			var itemSprite = new Sprite();
			itemSprite.Texture = item.GetNode<Sprite>("Sprite").Texture;
			itemSprite.RegionRect = item.GetNode<Sprite>("Sprite").RegionRect;
			itemSprite.RegionEnabled = item.GetNode<Sprite>("Sprite").RegionEnabled;
			itemSprite.Position = new Vector2(24, 24);
			itemSprite.Scale = new Vector2(0.75f, 0.75f);
			
			var itemButton = new Button();
			itemButton.AddChild(itemSprite);
			itemButton.RectSize = new Vector2(48, 48);
			itemButton.RectPosition = new Vector2(8, 8 + (index * 64));
			itemButton.Connect("pressed", this, "OnItemButtonPressed", new Godot.Collections.Array(item));
			
			BackpackButtons[index] = itemButton;
			
			EmitSignal("AddBackpackButton", itemButton, item);
		}
	}
	
	public void SetBackpackSize(int size)
	{
		Array.Resize(ref BackpackButtons, size);
	}
	
	private void OnItemButtonPressed(Node item)
	{
		item.Call("OnItemUse");
	}

	private void OnMessageKillTimerTimeout()
	{
		EmitSignal("MessageHide");
	}
}
