using Godot;
using System;

public class DynamicCollision : Area2D
{
	[Export] public int Aura = 4;
	[Export] public string AuraShape = "Rectangle";
	[Export] public string CollisionGroup = "";
	[Export] public string BeforeCallback = "";
	[Export] public string CollideCallback = "";
	[Export] public string AfterCallback = "";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		SetCollisionShape();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		if (Aura > 0)
		{
			var bodies = GetOverlappingBodies();
			if (bodies.Count > 0)
			{
				var parent = GetParent();
				if (parent.HasMethod(BeforeCallback))
					parent.Call(BeforeCallback, delta);
					
				if (parent.HasMethod(CollideCallback))
				{
					foreach (Node body in GetOverlappingBodies())
					{
						if (body.IsInGroup(CollisionGroup))
							parent.Call(CollideCallback, body, delta);
					}
				}
				if (parent.HasMethod(AfterCallback))
					parent.Call(AfterCallback, delta);
			}
		}
	}

	public void SetAura(int auraSize)
	{
		Aura = auraSize;
		SetCollisionShape();
	}

	public void SetAuraShape(string auraShape)
	{
		AuraShape = auraShape;
		SetCollisionShape();
	}

	// Build the collision area based on the aura and the parent's collision
	private void SetCollisionShape()
	{
		var parentShape = (CollisionShape2D)GetParent().GetNode("MainCollisionShape");
		
		GetNode<CollisionShape2D>("CollisionShape").Position = parentShape.Position;
		
		if (AuraShape == "Circle")
		{
			var newShape = new CircleShape2D();
			newShape.Radius = Aura;
			GetNode<CollisionShape2D>("CollisionShape").Shape = newShape;
		}
		else
		{
			var parentShapeShape = (RectangleShape2D)parentShape.Shape;
			var parentExtents = (Vector2)parentShapeShape.Extents;
			var newShape = new RectangleShape2D();
			newShape.Extents = new Vector2(parentExtents.x + Aura * 2, parentExtents.y + Aura * 2);
			GetNode<CollisionShape2D>("CollisionShape").Shape = newShape;
		}
	}
}
