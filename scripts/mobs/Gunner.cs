using Godot;
using System;

public class Gunner : Mob
{
	public void FriendlyCollisionBefore(float delta)
	{
		GetNode("Targeting").Call("ClearTargets");
	}

	public override void FriendlyCollision(Node body, float delta)
	{
		GetNode("Targeting").Call("AddTarget", body);
	}

	public void FriendlyCollisionAfter(float delta)
	{
		var targets = (Godot.Collections.Array)GetNode("Targeting").Call("GetTargets");
		foreach (Node target in targets)
		{
			var damageDealt = 0;
			if (target.IsInGroup("players"))
				damageDealt = Math.Max(Attack - (int)PlayerStats.Defense, Math.Min(1, Attack));
			else
				damageDealt = Math.Max(Attack - (int)target.Get("Defense"), Math.Min(1, Attack));
			if (damageDealt > 0)
				target.GetNode("Health").Call("TakeDamage", damageDealt * delta);
		}
	}
}
