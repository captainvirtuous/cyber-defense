using Godot;
using System;

public class ItemLibrary : Node
{
	[Export] public PackedScene Gun;
	[Export] public PackedScene Medkit;
	[Export] public PackedScene ResourcePack;

	public PackedScene[] Library;
	public Node[] Instances;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Library = new PackedScene[] 
		{
			ResourcePack,
			Gun,
			Medkit
		};
		Instances = new Node[] 
		{
			ResourcePack.Instance(),
			Gun.Instance(),
			Medkit.Instance()
		};
	}

	public Node GetItem(int index)
	{
		var item = Library[index];
		return item.Instance();
	}

	public Node GetInstance(int index)
	{
		return Instances[index];
	}
}
