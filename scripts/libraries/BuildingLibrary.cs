using Godot;
using System;

public class BuildingLibrary : Node
{
	[Export] public PackedScene Shrine;
	[Export] public PackedScene Tower;
	[Export] public PackedScene Wall;

	public PackedScene[] Library;
	public Node[] Instances;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Library = new PackedScene[] 
		{
			Wall,
			Tower,
			Shrine,
		};
		Instances = new Node[] 
		{
			Wall.Instance(),
			Tower.Instance(),
			Shrine.Instance(),
		};
	}

	public Node GetItem(int index)
	{
		var item = Library[index];
		return item.Instance();
	}

	public Node GetInstance(int index)
	{
		return Instances[index];
	}
}
