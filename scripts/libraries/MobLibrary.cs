using Godot;
using System;

public class MobLibrary : Node
{
	[Export] public PackedScene MobLevel1;
	[Export] public PackedScene Scout;
	[Export] public PackedScene Tank;
	[Export] public PackedScene Gunner;

	public PackedScene[] Library;
	public Node[] Instances;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Library = new PackedScene[] 
		{
			MobLevel1,
			Scout,
			Tank,
			Gunner
		};
		Instances = new Node[] 
		{
			MobLevel1.Instance(),
			Scout.Instance(),
			Tank.Instance(),
			Gunner.Instance()
		};
	}

	public Node GetItem(int index)
	{
		var item = Library[index];
		return item.Instance();
	}

	public Node GetInstance(int index)
	{
		return Instances[index];
	}
}
