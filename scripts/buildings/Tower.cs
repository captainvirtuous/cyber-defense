using Godot;
using System;
using System.Collections.Generic;

public class Tower : Building
{
	public void MobCollisionBefore(float delta)
	{
		GetNode("Targeting").Call("ClearTargets");
	}

	public override void MobCollision(Node body, float delta)
	{
		if (Built)
			GetNode("Targeting").Call("AddTarget", body);
	}

	public void MobCollisionAfter(float delta)
	{
		var targets = (Godot.Collections.Array)GetNode("Targeting").Call("GetTargets");
		foreach (Node target in targets)
		{
			var damageDealt = Math.Max(Attack - (int)target.Get("Defense"), Math.Min(1, Attack));
			if (damageDealt > 0)
				target.GetNode("Health").Call("TakeDamage", damageDealt * delta);
		}
	}
}
