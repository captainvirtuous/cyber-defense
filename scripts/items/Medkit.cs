using Godot;
using System;

public class Medkit : Item
{
	public int Heal = 50;

	public override void PlayerCollision(Node body, float delta)
	{
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("PickupItem", this.Duplicate());
		QueueFree();
	}

	public override void ItemPurchased(Node body)
	{
		body.GetNode("Inventory").Call("PickupItem", this.Duplicate());
	}
	
	public override void OnItemUse()
	{
		GetNode("/root/PlayerStats").Call("ModifyPlayerHealth", Heal);
		GetNode("/root/PlayerStats").GetNode("Inventory").Call("DropItem", BackpackSlot);
		QueueFree();
	}
}
