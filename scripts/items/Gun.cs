using Godot;
using System;

public class Gun : Weapon
{
	public override void EquipItem()
	{
		base.EquipItem();
	}
	
	public override void UnequipItem()
	{
		base.UnequipItem();
	}

	public override void MobCollisionBefore(float delta)
	{
		GetNode("Targeting").Call("ClearTargets");
	}

	public override void MobCollision(Node body, float delta)
	{
		GetNode("Targeting").Call("AddTarget", body);
	}

	public override void MobCollisionAfter(float delta)
	{
		var targets = (Godot.Collections.Array)GetNode("Targeting").Call("GetTargets");
		foreach (Node target in targets)
		{
			var damageDealt = Math.Max(PlayerStats.Attack - (int)target.Get("Defense"), Math.Min(1, PlayerStats.Attack));
			if (damageDealt > 0)
				target.GetNode("Health").Call("TakeDamage", damageDealt * delta);
		}
	}
}
