using Godot;
using System;

public class ResourcePack : Item
{
	public int Metal = 0;
	public int Circuitry = 0;
	public int Energy = 0;
	public int Cred = 0;

	public override void PlayerCollision(Node body, float delta)
	{
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectMetal", Metal);
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectCircuitry", Circuitry);
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectEnergy", Energy);
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectCred", Cred);
		QueueFree();
	}

	public override void ItemPurchased(Node body)
	{
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectMetal", Metal);
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectCircuitry", Circuitry);
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectEnergy", Energy);
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectCred", Cred);
	}
}
