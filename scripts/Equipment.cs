using Godot;
using System;

public class Equipment : Item
{
	[Export] public string Slot = "";
	[Export] public int ModAttack = 0;
	[Export] public int ModDefense = 0;
	[Export] public int ModHeal = 0;
	[Export] public int ModMaxHP = 0;
	[Export] public int ModSpeed = 0;

	[Export] public int ModMaxStamina = 0;
	[Export] public int ModStaminaDrain = 0;
	[Export] public int ModStaminaRegen = 0;

	[Export] public int ModBuildRange = 0;
	
	public bool Equipped = false;
	
	public override void OnItemUse()
	{
		if (Equipped)
			UnequipItem();
		else
			EquipItem();
	}
	
	public virtual void EquipItem()
	{
		var isSlotEquipped = (bool)GetNode("/root/PlayerStats").Call("CheckSlotEquipped", Slot);
		if (!isSlotEquipped)
		{
			Equipped = true;
			GetNode("/root/PlayerStats").Call("EquipSlot", Slot);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Attack", ModAttack);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Defense", ModDefense);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Heal", ModHeal);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "MaxHP", ModMaxHP);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Speed", ModSpeed);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "MaxStamina", ModMaxStamina);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "StaminaDrain", ModStaminaDrain);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "StaminaRegen", ModStaminaRegen);
			GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "BuildRange", ModBuildRange);
			GetNode("/root/GUIData").Call("SetMessage", "You equipped " + ItemName + "!", 2);
		}
	}
	
	public virtual void UnequipItem()
	{
		Equipped = false;
		GetNode("/root/PlayerStats").Call("UnequipSlot", Slot);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Attack", -ModAttack);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Defense", -ModDefense);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Heal", -ModHeal);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "MaxHP", -ModMaxHP);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "Speed", -ModSpeed);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "MaxStamina", -ModMaxStamina);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "StaminaDrain", -ModStaminaDrain);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "StaminaRegen", -ModStaminaRegen);
		GetNode("/root/PlayerStats").Call("ModifyPlayerStats", "BuildRange", -ModBuildRange);
			GetNode("/root/GUIData").Call("SetMessage", "You unequipped " + ItemName + "!", 2);
	}
}
