using Godot;
using System;

public class Player : KinematicBody2D
{
	[Signal] public delegate void Build(Vector2 position);
	[Signal] public delegate void PlayerKilled();

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode("Health").Call("UpdateMaxHealth", PlayerStats.MaxHP, true);
		GetNode("Stamina").Call("UpdateMaxStamina", PlayerStats.MaxStamina, true);
		GetNode("/root/PlayerStats").Connect("ModifyHealth", this, "OnPlayerStatsModifyHealth");
		GetNode("/root/PlayerStats").Connect("ModifyMaxHP", this, "OnPlayerStatsModifyMaxHP");
		GetNode("/root/PlayerStats").Connect("ModifyMaxStamina", this, "OnPlayerStatsModifyMaxStamina");
		GetNode("/root/PlayerStats").Connect("WeaponRange", this, "OnPlayerWeaponRange");
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		HandleMovement(delta);
		QuickChange();
		PrimaryAction();
	}

	// Move and animate the player sprite
	private void HandleMovement(float delta)
	{
		// The player's movement vector.
		var velocity = new Vector2();
		if (Input.IsActionPressed("ui_right"))
			velocity.x += 1;
		else if (Input.IsActionPressed("ui_left"))
			velocity.x -= 1;
		if (Input.IsActionPressed("ui_down"))
			velocity.y += 1;
		else if (Input.IsActionPressed("ui_up"))
			velocity.y -= 1;

		// Modify animation based on movement direction
		if (Input.IsActionPressed("ui_right"))
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "right";
		else if (Input.IsActionPressed("ui_left"))
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "left";
		else if (Input.IsActionPressed("ui_down"))
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "down";
		else if (Input.IsActionPressed("ui_up"))
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "up";

		// Only animate if there is movement 
		if (velocity.Length() > 0)
			GetNode<AnimatedSprite>("AnimatedSprite").Play();
		else
			GetNode<AnimatedSprite>("AnimatedSprite").Stop();

		// Make the player run
		var moveSpeed = (int)GetNode("/root/PlayerStats").Get("Speed");
		if (Input.IsActionPressed("ui_run") && !(bool)GetNode("Stamina").Call("Drain", (int)GetNode("/root/PlayerStats").Get("StaminaDrain") * delta))
				moveSpeed *= 2;

		// Restore stamina
		if (!Input.IsActionPressed("ui_run") || velocity.Length() == 0)
			GetNode("Stamina").Call("Regen", (int)GetNode("/root/PlayerStats").Get("StaminaRegen") * delta);

		velocity = velocity.Normalized() * moveSpeed;
		velocity = MoveAndSlide(velocity);
	}

	// Perform player primary action
	private void PrimaryAction()
	{
		// Trigger build
		if (Input.IsActionJustPressed("ui_accept"))
		{
			// Get the mouse position and convert it to the world coordinates
			var mousePosition = (Vector2)(GetViewport().CanvasTransform.AffineInverse() * GetViewport().GetMousePosition());
			if (Position.DistanceTo(mousePosition) <= (int)GetNode("/root/PlayerStats").Get("BuildRange"))
				EmitSignal("Build", mousePosition);
		}
	}

	// Change what the player has equiped
	private void QuickChange()
	{
		var equipPrimary = PlayerStats.EquipPrimary;
		if (Input.IsActionJustPressed("hotkey_1"))
		{
			equipPrimary = 0;
		}
		else if (Input.IsActionJustPressed("hotkey_2"))
		{
			equipPrimary = 1;
		}
		else if (Input.IsActionJustPressed("hotkey_3"))
		{
			equipPrimary = 2;
		}
		else if (Input.IsActionJustReleased("ui_page_down"))
		{
			equipPrimary += 1;
			if (equipPrimary > 2)
				equipPrimary = 0;
		}
		else if (Input.IsActionJustReleased("ui_page_up"))
		{
			equipPrimary -= 1;
			if (equipPrimary < 0)
				equipPrimary = 2;
		}
		PlayerStats.EquipPrimary = equipPrimary;

		var building = (Node2D)GetNode("/root/BuildingLibrary").Call("GetInstance", equipPrimary);
		var buildingSprite = building.GetNode<AnimatedSprite>("AnimatedSprite");
		GetNode("/root/GUIData").Call("SetBuilding", buildingSprite);
	}

	// Player is killed
	public void Kill()
	{
		EmitSignal("PlayerKilled");
	}

	// Interact with the Mob within collision
	public void MobCollisionBefore(float delta)
	{
		var weapon = (Node2D)GetNode("/root/PlayerStats").Get("Weapon");
		if (weapon != null)
		{
			weapon.Call("MobCollisionBefore", delta);
		}
	}

	public void MobCollision(Node body, float delta)
	{
		var weapon = (Node2D)GetNode("/root/PlayerStats").Get("Weapon");
		if (weapon != null)
		{
			weapon.Call("MobCollision", body, delta);
		}
		else
		{
			var damageDealt = Math.Max(PlayerStats.Attack - (int)body.Get("Defense"), Math.Min(1, PlayerStats.Attack));
			if (damageDealt > 0)
				body.GetNode("Health").Call("TakeDamage", damageDealt * delta);
		}
	}
	
	public void MobCollisionAfter(float delta)
	{
		var weapon = (Node2D)GetNode("/root/PlayerStats").Get("Weapon");
		if (weapon != null)
		{
			weapon.Call("MobCollisionAfter", delta);
		}
	}

	// Interact with Building within collision
	public void BuildingCollision(Node body, float delta)
	{
		body.GetNode("Health").Call("RestoreHealth", PlayerStats.Heal * delta);
	}
	
	public void OnPlayerStatsModifyHealth(int amount)
	{
		if (amount > 0)
			GetNode("Health").Call("RestoreHealth", amount);
		else if (amount < 0)
			GetNode("Health").Call("TakeDamage", -amount);
	}
	
	public void OnPlayerStatsModifyMaxHP()
	{
		GetNode("Health").Call("UpdateMaxHealth", PlayerStats.MaxHP, false);
	}
	
	public void OnPlayerStatsModifyMaxStamina()
	{
		GetNode("Stamina").Call("UpdateMaxStamina", PlayerStats.MaxStamina, false);
	}
	
	public void OnPlayerWeaponRange(int WeaponRange, string WeaponRangeShape)
	{
		GetNode("MobCollision").Call("SetAura", WeaponRange);
		GetNode("MobCollision").Call("SetAuraShape", WeaponRangeShape);
	}
}
