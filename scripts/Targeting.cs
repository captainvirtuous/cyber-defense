using Godot;
using System;
using System.Collections.Generic;

public class Targeting : Node
{
	[Export] public string TargetMethod = "lowest_health";
	[Export] public int TargetCount = 1;
	
	private List<Node> Targets = new List<Node>();

	public void ClearTargets()
	{
		Targets.Clear();
	}

	public List<Node> GetTargets()
	{
		return Targets;
	}
	
	public void CalculateTargetsFromList(Node[] bodies)
	{
		foreach(Node body in bodies)
		{
			AddTarget(body);
		}
	}

	public void AddTarget(Node body)
	{
		if (Targets.Count < TargetCount)
		{
			Targets.Add(body);
		}
		else
		{
			var replacement = 0;
			var replacementFound = false;
			switch (TargetMethod)
			{
				case "lowest_health":
					// Loop through current targets to get the one with the highest health to replace
					for(int i = 0; i < Targets.Count; i++)
					{
						if (IsLowerHealth(Targets[replacement], Targets[i]))
						{
							replacementFound = true;
							replacement = i;
						}
					}
					break;
				case "highest_health":
					// Loop through current targets to get the one with the lowest health to replace
					for(int i = 0; i < Targets.Count; i++)
					{
						if (IsLowerHealth(Targets[i], Targets[replacement]))
						{
							replacementFound = true;
							replacement = i;
						}
					}
					break;
				case "player":
					// Only check to replace a target if the body is a player
					if(body.IsInGroup("players"))
					{
						// Loop through current targets to see if it's not a player
						for(int i = 0; i < Targets.Count && !replacementFound; i++)
						{
							if (!Targets[i].IsInGroup("players"))
							{
								replacementFound = true;
								replacement = i;
							}
						}
					}
					break;
				default:
					// Do not replace targets
					break;
			}
			
			// Replace target with new body
			if (replacementFound)
				Targets[replacement] = body;
		}
	}

	// Does body1 have lower health than body2
	public bool IsLowerHealth(Node body1, Node body2)
	{
		return body2.GetNode<ProgressBar>("Health").Value > body1.GetNode<ProgressBar>("Health").Value;
	}
}
