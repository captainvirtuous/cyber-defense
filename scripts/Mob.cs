using Godot;
using System;

public class Mob : KinematicBody2D
{
	public float MovementDirection;
	public Vector2 MovementDestination;

	[Export] public int Speed = 0;
	[Export] public int Power = 0;
	[Export] public int Attack = 0;
	[Export] public int Defense = 0;
	[Export] public int MaxHP = 0;
	
	[Export] public int Metal = 0;
	[Export] public int Circuitry = 0;
	[Export] public int Energy = 0;
	[Export] public int Cred = 0;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode("Inventory").Call("CollectMetal", Metal);
		GetNode("Inventory").Call("CollectCircuitry", Circuitry);
		GetNode("Inventory").Call("CollectEnergy", Energy);
		GetNode("Inventory").Call("CollectCred", Cred);
		
		if (MovementDirection >= -3f * Math.PI / 4f && MovementDirection <= -Math.PI / 4f)
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "up";
		else if (MovementDirection >= -Math.PI / 4f && MovementDirection <= Math.PI / 4f)
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "right";
		else if (MovementDirection >= Math.PI / 4f && MovementDirection <= 3f * Math.PI / 4f)
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "down";
		else
			GetNode<AnimatedSprite>("AnimatedSprite").Animation = "left";
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		var velocity = (MovementDestination - Position).Normalized() * Speed;
		MoveAndCollide(velocity * delta);
	}
	
	public virtual void FriendlyCollision(Node body, float delta)
	{
		var damageDealt = 0;
		if (body.IsInGroup("players"))
			damageDealt = Math.Max(Attack - (int)PlayerStats.Defense, Math.Min(1, Attack));
		else
			damageDealt = Math.Max(Attack - (int)body.Get("Defense"), Math.Min(1, Attack));
		if (damageDealt > 0)
			body.GetNode("Health").Call("TakeDamage", damageDealt * delta);
	}
	
	public virtual void Kill()
	{
		var items = (Godot.Collections.Array)GetNode("Inventory").Call("DropAll");
		foreach(Node2D item in items)
		{
			item.Position = Position;
			GetParent().AddChild(item);
		}
		QueueFree();
	}
}
