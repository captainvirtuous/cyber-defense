using Godot;
using System;
using System.Collections.Generic;

public class Main : Node
{
	public Rect2 BuildArea;
	public List<string> Buildings = new List<string>();
	public int WavePowerLimit = 10;
	public int WavePower = 0;
	public int Wave = 0;
	public int MobTypeCount = 0;
	
	private RandomNumberGenerator RNG = new RandomNumberGenerator();

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		var mobLibrary = (Godot.Collections.Array)GetNode("/root/MobLibrary").Get("Library");
		MobTypeCount = (int)mobLibrary.Count;
		var buildAreaNode = (Area2D)GetNode<Area2D>("BuildArea");
		var buildShapeShape = (RectangleShape2D)buildAreaNode.GetNode<CollisionShape2D>("BuildShape").Shape;
		var buildShapeExtents = (Vector2)buildShapeShape.Extents;
		BuildArea = new Rect2(buildAreaNode.Position - buildShapeExtents, buildShapeExtents * 2);
		GetNode("/root/GUIData").Call("SetMessage", "Check-in at base to prepare for the onslaught!", 0);
		GetNode("/root/GUIData").Call("HideWaveCountdown");
		GetNode("/root/GUIData").Call("SetWave", Wave);
	}

	private void OnWaveTimerTimeout()
	{
		WavePower = 0;
		GetNode("/root/GUIData").Call("SetMessage", "Here they come!", 2);
		GetNode("/root/GUIData").Call("SetWave", Wave);
		GetNode("/root/GUIData").Call("HideWaveCountdown");
		GetNode<Timer>("SpawnTimer").Start();
	}

	private void OnSpawnTimerTimeout()
	{
		if (WavePower >= WavePowerLimit)
		{
			Wave += 1;
			WavePowerLimit = (int)Math.Ceiling(WavePowerLimit * 1.5);
			GetNode("/root/GUIData").Call("SetMessage", "Looks like a break, but the next group comes in " + GetNode<Timer>("WaveTimer").WaitTime + "!", 3);
			GetNode("/root/GUIData").Call("SetWaveCountdown", GetNode("WaveTimer"));
			GetNode<Timer>("SpawnTimer").Stop();
			GetNode<Timer>("WaveTimer").WaitTime += 2;
			GetNode<Timer>("WaveTimer").Start();
		}
		else
		{
			var mobSpawnLocation = GetNode("MobSpawnPath").GetNode<PathFollow2D>("MobSpawnLocation");
			var homePointPosition = (Vector2)GetNode<Position2D>("HomePoint").Position;
			
			// Spawn enemies of every type
			for (int mi = 0; mi < MobTypeCount && WavePower < WavePowerLimit; mi++)
			{
				var spawnAmount = (int)Math.Ceiling((float)Wave / (mi + 1));
				var mobInfo = (Node2D)GetNode("/root/MobLibrary").Call("GetInstance", mi);
				WavePower += (spawnAmount * (int)mobInfo.Get("Power"));
				
				// Set a random amount of enemies to spawn each timeout
				for (int i = 0; i < spawnAmount; i++)
				{
					// Choose a random location on Path2D.
					mobSpawnLocation.Offset = RNG.Randi();
					// Create a Mob instance.
					var mob = (Node2D)GetNode("/root/MobLibrary").Call("GetItem", mi);
					// Set the mob's position to a random location.
					mob.Position = mobSpawnLocation.Position;
					mob.Set("MovementDestination", homePointPosition);
					mob.Set("MovementDirection", homePointPosition.AngleToPoint(mobSpawnLocation.Position));
					// Add the mob to the scene
					AddChild(mob);
				}
			}
		}
	}

	private void OnPlayerBuild(Vector2 position)
	{
		var tilePosition = (Vector2)GetNode("WorldTiles").Call("GetTilePosition", position);
		var tileKey = tilePosition.x.ToString() + '_' + tilePosition.y.ToString();
		if (BuildArea.HasPoint(position) && !Buildings.Contains(tileKey))
		{
			// Get the building data and player inventory to validate that it can be built
			var playerInventory = (Node)GetNode("/root/PlayerStats").GetNode("Inventory");
			var buildingData = (Node)GetNode("/root/BuildingLibrary").Call("GetInstance", GetNode("/root/PlayerStats").Get("EquipPrimary"));
			
			if ((int)buildingData.Get("Metal") <= (int)playerInventory.Get("Metal") && 
				(int)buildingData.Get("Circuitry") <= (int)playerInventory.Get("Circuitry") && 
				(int)buildingData.Get("Energy") <= (int)playerInventory.Get("Energy"))
			{
				// Spend the player's resources
				playerInventory.Call("SpendMetal", (int)buildingData.Get("Metal"));
				playerInventory.Call("SpendCircuitry", (int)buildingData.Get("Circuitry"));
				playerInventory.Call("SpendEnergy", (int)buildingData.Get("Energy"));
				
				// Get the building and add it to the scene.
				var building = (Node2D)GetNode("/root/BuildingLibrary").Call("GetItem", GetNode("/root/PlayerStats").Get("EquipPrimary"));
				
				// Add the building to the scene.
				building.Position = tilePosition;
				building.Set("TileKey", tileKey);
				building.Connect("Destroy", this, "OnBuildingDestroy");
				AddChild(building);
				Buildings.Add(tileKey);
			}
			else
			{
				GetNode("/root/GUIData").Call("SetMessage", "Gather some scraps to build that!", 2);
			}
		}
	}

	private void OnPlayerPlayerKilled()
	{
		GameOver();
	}

	private void OnBaseBuildBase()
	{
		GameStart();
	}

	private void OnBaseDestroyBase()
	{
		GameOver();
	}

	private void OnBuildingDestroy(string tileKey)
	{
		Buildings.Remove(tileKey);
	}

	private void OnShopOpenShop()
	{
		GetNode("/root/GUIData").Call("SetMessage", "I got the goods if you got the cred!", 2);
	}

	private void OnShopCloseShop()
	{
		GetNode("/root/GUIData").Call("SetMessage", "Later gator!", 2);
	}
	
	private void OnShopPurchase(Node item)
	{
		var itemValue = (int)item.Get("CredValue");
		if ((int)GetNode("/root/PlayerStats").GetNode("Inventory").Get("Cred") >= (int)item.Get("CredValue"))
		{
			GetNode("/root/PlayerStats").GetNode("Inventory").Call("SpendCred", itemValue);
			GetNode("Shop").GetNode("Inventory").Call("CollectCred", itemValue);
			item.Call("ItemPurchased", GetNode("/root/PlayerStats"));
		}
		else {
			GetNode("/root/GUIData").Call("SetMessage", "Get some more cred you bum!", 2);
		}
	}

	public void GameOver()
	{
		GetNode("/root/GUIData").Call("SetMessage", "You lose chummer!\nRepair your base to retry!", 0);
		GetNode<Timer>("SpawnTimer").Stop();
		GetNode<Timer>("WaveTimer").Stop();
		GetNode("Base").Set("Destroyed", true);
		GetNode("/root/PlayerStats").Set("Killed",  true);
		GetTree().CallGroup("mobs", "queue_free");
		GetTree().CallGroup("items", "queue_free");
		GetNode("/root/PlayerStats").GetNode("Inventory").Call("DestroyAll");
	}

	public void GameStart()
	{
		GetNode("/root/GUIData").Call("SetMessage", "Locked and loaded! Prepare your defenses!", 5);
		GetTree().CallGroup("non_base_buildings", "queue_free");
		Buildings = new List<string>((string[])GetNode("Base").Get("BaseLocation"));
		Wave = 1;
		WavePowerLimit = 10;
		GetNode("/root/GUIData").Call("SetWave", Wave);
		GetNode("/root/GUIData").Call("SetWaveCountdown", GetNode("WaveTimer"));
		GetNode("Base").Set("Destroyed", false);
		PlayerStats.Killed = false;
		GetNode("Base").GetNode("Health").Call("FullHealth");
		GetNode("Player").GetNode("Health").Call("FullHealth");
		GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectMetal", 1000);
		GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectCircuitry", 1000);
		GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectEnergy", 1000);
//		GetNode("/root/PlayerStats").GetNode("Inventory").Call("CollectCred", 10000);
		GetNode<Timer>("WaveTimer").WaitTime = 5;
		GetNode<Timer>("WaveTimer").Start();
	}
}
