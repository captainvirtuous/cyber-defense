using Godot;
using System;

public class GUI : CanvasLayer
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode("/root/GUIData").Connect("WaveLabel", this, "OnGUIDataWaveLabel");
		GetNode("/root/GUIData").Connect("WaveCountdownShow", this, "OnGUIDataWaveCountdownShow");
		GetNode("/root/GUIData").Connect("WaveCountdownHide", this, "OnGUIDataWaveCountdownHide");
		GetNode("/root/GUIData").Connect("MessageLabel", this, "OnGUIDataMessageLabel");
		GetNode("/root/GUIData").Connect("MessageHide", this, "OnGUIDataMessageHide");
		GetNode("/root/GUIData").Connect("SelectedBuildingSprite", this, "OnGUIDataSelectedBuildingSprite");
		GetNode("/root/GUIData").Connect("MetalAmountLabel", this, "OnGUIDataMetalAmountLabel");
		GetNode("/root/GUIData").Connect("CircuitryAmountLabel", this, "OnGUIDataCircuitryAmountLabel");
		GetNode("/root/GUIData").Connect("EmergyAmountLabel", this, "OnGUIDataEmergyAmountLabel");
		GetNode("/root/GUIData").Connect("CredAmountLabel", this, "OnGUIDataCredAmountLabel");
		GetNode("/root/GUIData").Connect("AddBackpackButton", this, "OnGUIDataAddBackpackButton");
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		var waveTimer = (Timer)GetNode("/root/GUIData").Get("WaveTimer");
		if (waveTimer != null && waveTimer.TimeLeft > 0f)
			GetNode<Label>("WaveCountdown").Text = Math.Ceiling(waveTimer.TimeLeft) + " seconds...";
	}
	
	public void OnGUIDataWaveLabel(string text)
	{
		GetNode<Label>("Wave").Text = text;
	}
	
	public void OnGUIDataWaveCountdownShow()
	{
		GetNode<Label>("WaveCountdown").Show();
	}
	
	public void OnGUIDataWaveCountdownHide()
	{
		GetNode<Label>("WaveCountdown").Text = "";
		GetNode<Label>("WaveCountdown").Show();
	}
	
	public void OnGUIDataMessageLabel(string text)
	{
		GetNode<Label>("Message").Text = text;
		GetNode<Label>("Message").Show();
	}
	
	public void OnGUIDataMessageHide()
	{
		GetNode<Label>("Message").Hide();
	}
	
	public void OnGUIDataSelectedBuildingSprite(AnimatedSprite sprite)
	{
		GetNode("SelectedBuilding").GetNode<AnimatedSprite>("BuildingSprite").Frames = sprite.Frames;
	}
	
	public void OnGUIDataMetalAmountLabel(string text)
	{
		GetNode<Label>("MetalAmount").Text = text;
	}
	
	public void OnGUIDataCircuitryAmountLabel(string text)
	{
		GetNode<Label>("CircuitryAmount").Text = text;
	}
	
	public void OnGUIDataEmergyAmountLabel(string text)
	{
		GetNode<Label>("EnergyAmount").Text = text;
	}
	
	public void OnGUIDataCredAmountLabel(string text)
	{
		GetNode<Label>("CredAmount").Text = text;
	}
	
	public void OnGUIDataAddBackpackButton(Button itemButton, Node item)
	{
			GetNode<Panel>("BackpackPanel").AddChild(itemButton);
			GetNode("HiddenItems").AddChild(item);
	}
}
