using Godot;
using System;

public class Base : StaticBody2D
{
	[Signal] public delegate void DestroyBase();
	[Signal] public delegate void BuildBase();

	public string[] BaseLocation = new string[]
	{
		"-64_0",
		"0_0",
		"0_-64",
		"-64_-64"
	};

	public int Attack = 10;
	public int Defense = 0;
	public int Heal = 5;
	public int MaxHP = 200;

	public bool Destroyed = true;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode<ProgressBar>("Health").Value = 0;
	}

	public void Kill()
	{
		EmitSignal("DestroyBase");
	}

	public void MobCollision(Node body, float delta)
	{
		var damageDealt = Math.Max(Attack - (int)body.Get("Defense"), Math.Min(1, Attack));
		if (damageDealt > 0)
			body.GetNode("Health").Call("TakeDamage", damageDealt * delta);
	}

	public void PlayerCollision(Node body, float delta)
	{
		if (Destroyed || (bool)PlayerStats.Killed)
			EmitSignal("BuildBase");
		else
			body.GetNode("Health").Call("RestoreHealth", Heal * delta);
	}
}
