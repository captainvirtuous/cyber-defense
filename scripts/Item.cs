using Godot;
using System;

public class Item : Area2D
{
	[Export] public int CredValue = 0;
	[Export] public string ItemName = "";
	
	public int BackpackSlot = -1;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public virtual void PlayerCollision(Node body, float delta)
	{
		body.GetNode("/root/PlayerStats").GetNode("Inventory").Call("PickupItem", this.Duplicate());
		QueueFree();
	}

	public virtual void ItemPurchased(Node body)
	{
		body.GetNode("Inventory").Call("PickupItem", this.Duplicate());
	}
	
	public virtual void OnItemUse()
	{
		GetNode("/root/PlayerStats").GetNode("Inventory").Call("DropItem", BackpackSlot);
		QueueFree();
	}

	private void OnDespawnTimerTimeout()
	{
		QueueFree();
	}
}
