using Godot;
using System;

public class Weapon : Equipment
{
	[Export] public int WeaponRange = 4;
	[Export] public string WeaponRangeShape = "Rectangle";

	public override void EquipItem()
	{
		base.EquipItem();
		GetNode("/root/PlayerStats").Call("EquipWeapon", this);
	}
	
	public override void UnequipItem()
	{
		base.UnequipItem();
		GetNode("/root/PlayerStats").Call("UnequipWeapon");
	}
	
	public virtual void MobCollisionBefore(float delta)
	{
		GD.Print("BEFORE");
	}
	
	public virtual void MobCollision(Node body, float delta)
	{
		GD.Print("COLLIDE");
	}
	
	public virtual void MobCollisionAfter(float delta)
	{
		GD.Print("BANG");
	}
}
