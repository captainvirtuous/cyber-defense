using Godot;
using System;

public class Building : StaticBody2D
{
	[Signal] public delegate void Destroy(string tileKey);

	[Export] public int Attack = 0;
	[Export] public int Defense = 0;
	[Export] public int Heal = 0;
	[Export] public int MaxHP = 0;

	[Export] public int BuildTime = 1;
	[Export] public int DestroyTime = 1;
	
	[Export] public int Metal = 0;
	[Export] public int Circuitry = 0;
	[Export] public int Energy = 0;

	public string TileKey = "";
	public bool Built = false;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode<ProgressBar>("Health").Set("DamageAmplifier", 4f);
		GetNode<ProgressBar>("BuildProgress").Value = 0;
		GetNode<Timer>("BuildTimer").WaitTime = BuildTime;
		GetNode<Timer>("BuildTimer").Start();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		// Build / Dismantle progress bar
		if (GetNode<Timer>("BuildTimer").TimeLeft > 0)
		{
			if (Built)
				GetNode<ProgressBar>("BuildProgress").Value = (GetNode<Timer>("BuildTimer").TimeLeft / GetNode<Timer>("BuildTimer").WaitTime) * 100;
			else
				GetNode<ProgressBar>("BuildProgress").Value = ((GetNode<Timer>("BuildTimer").WaitTime - GetNode<Timer>("BuildTimer").TimeLeft) / GetNode<Timer>("BuildTimer").WaitTime) * 100;
		}
	}

	private void OnBuildTimerTimeout()
	{
		GetNode<ProgressBar>("BuildProgress").Hide();
		if (Built)
		{
			EmitSignal("Destroy", TileKey);
			QueueFree();
		}
		else
		{
			Built = true;
			GetNode<ProgressBar>("Health").Set("DamageAmplifier", 1f);
		}
	}

	private void OnBuildingInputEvent(object viewport, object @event, int shape_idx)
	{
		if (@event is InputEventMouseButton eventKey && eventKey.Pressed && eventKey.ButtonIndex == (int)ButtonList.Right)
			DismantleBuilding();
	}

	public void DismantleBuilding()
	{
		if (GetNode<Timer>("BuildTimer").TimeLeft <= 0)
		{
			GetNode<Timer>("BuildTimer").WaitTime = DestroyTime;
			GetNode<Timer>("BuildTimer").Start();
			GetNode<ProgressBar>("BuildProgress").Show();
		}
	}
	
	public virtual void Kill()
	{
		EmitSignal("Destroy", TileKey);
		QueueFree();
	}

	public virtual void MobCollision(Node body, float delta)
	{
		if (Built)
		{
			var damageDealt = Math.Max(Attack - (int)body.Get("Defense"), Math.Min(1, Attack));
			if (damageDealt > 0)
				body.GetNode("Health").Call("TakeDamage", damageDealt * delta);
		}
	}

	public virtual void PlayerCollision(Node body, float delta)
	{
		if (Built)
			body.GetNode("Health").Call("RestoreHealth", Heal * delta);
	}
}
