using Godot;
using System;

public class WorldTiles : TileMap
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public Vector2 GetTilePosition(Vector2 worldPosition)
	{
		return MapToWorld(WorldToMap(worldPosition));
	}
}
