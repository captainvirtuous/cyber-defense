using Godot;
using System;

public class Health : ProgressBar
{
	public float DamageAmplifier = 1f;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		if(GetParent().Get("MaxHP") == null)
			this.MaxValue = 0;
		else
			this.MaxValue = (int)GetParent().Get("MaxHP");
		this.Value = this.MaxValue;
	}

	// Health takes damage
	public void TakeDamage(float damage)
	{
		this.Value -= damage * DamageAmplifier;
		if (this.Value <= 0)
		{
			var parent = (Node)GetParent();
			if (parent.HasMethod("Kill"))
				parent.Call("Kill");
			else
				parent.QueueFree();
		}
	}

	// Health is restored
	public void RestoreHealth(float restore)
	{
		if (this.Value < this.MaxValue)
			this.Value += restore;
	}
	
	// Fill health to 100%
	public void FullHealth()
	{
		this.Value = this.MaxValue;
	}
	
	public void UpdateMaxHealth(int health, bool fill)
	{
		this.MaxValue = health;
		if (fill)
			this.Value = this.MaxValue;
	}
}
